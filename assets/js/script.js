/* ==========================================================================
 * script.js
 * Javascript to be utilized throughout the site
 * ========================================================================== */
 
$(document).ready(function() {

	//navburger
    $('.lines-button').on('click', function( ) {
    	$('.main-nav .container').toggleClass("grey-bg", 100);
    	$( ".nav-links-container" ).slideToggle();
    });
    var anchor = document.querySelectorAll('.nav-assets button');
    [].forEach.call(anchor, function(anchor){
      var open = false;
      anchor.onclick = function(event){
        event.preventDefault();
        if(!open){
          this.classList.add('close');
          open = true;
        }
        else{
          this.classList.remove('close');
          open = false;
        }
      };
    }); 

    //overview hero area square animations


    //form specific js
    $('#datepickerExample').datepicker();
    $('[data-toggle="tooltip"]').tooltip();


	function selectElementContents(el) {
	    if (window.getSelection && document.createRange) {
	        // IE 9 and non-IE
	        var range = document.createRange();
	        range.selectNodeContents(el);
	        var sel = window.getSelection();
	        sel.removeAllRanges();
	        sel.addRange(range);
	    } else if (document.body.createTextRange) {
	        // IE < 9
	        var textRange = document.body.createTextRange();
	        textRange.moveToElementText(el);
	        textRange.select();
	    }
	}




	$('.sg-btn--source').each( function() {
	    $(this).on('click', function() {
	        $('.sg-source').slideToggle();
		});
	});

	$('.sg-btn--select').each( function() {
	    $(this).on('click', function() {
	       selectElementContents(document.getElementById('test'));
		});
	});



	
	/* navburger functionality */
	$('#nav-toggle').on('click', function(e) {
		$(this).toggleClass('navburger-active'); //TODO make sure this works. no internet on train.
		e.preventDefault();
	});

	/* menu scroll highlight */
	// Cache selectors
	var lastId,

	topMenu = $("#page-nav"),
	topMenuHeight = topMenu.outerHeight()+15,
	// All list items
	menuItems = topMenu.find("a"),
	// Anchors corresponding to menu items
	scrollItems = menuItems.map(function(){
		var item = $($(this).attr("href"));
		if (item.length) { return item; }
	});

	// Bind click handler to menu items
	// so we can get a fancy scroll animation
	menuItems.click(function(e){
		var href = $(this).attr("href"),
     	offsetTop = href === "#" ? 0 : $(href).offset().top-topMenuHeight+1;
  		$('html, body').stop().animate({ 
      		scrollTop: offsetTop
  		}, 300);
  		e.preventDefault();
	});

	// Bind to scroll
	$(window).scroll(function(){
		// Get container scroll position
   		var fromTop = $(this).scrollTop()+topMenuHeight;
   
   		// Get id of current scroll item
   		var cur = scrollItems.map(function(){
     		if ($(this).offset().top < fromTop) {
       			return $(this);
       		}
   		});
   		// Get the id of the current element
   		cur = cur[cur.length-1];
   		var id = cur && cur.length ? cur[0].id : "";
   
		if (lastId !== id) {
	       	lastId = id;
	     	// Set/remove active class
	       	menuItems.parent().removeClass("active").end().filter("[href=#"+id+"]").parent().addClass("active");
		}                   
	});

	

	//Page specific functions
	var pathname = window.location.pathname;

	//********* Overview ***********//
	if(pathname === "/index.html" || pathname === "/"){

		$.getScript( "vendor/box-api/crocodoc.viewer.js", function() {
			/* jshint ignore:start */
			var viewer,
	            url = 'https://view-api.box.com/1/sessions/8ae41a3a4bce4ddaafeb4f5a77310d1d/assets';

	        viewer = Crocodoc.createViewer('.viewer', {
	            url: url,
	            layout: Crocodoc.LAYOUT_PRESENTATION
	        });
	        viewer.load();

	        viewer.on('ready', function () {
	            $(window).on('keydown', function (ev) {
	                if (ev.keyCode === 37) {
	                    viewer.scrollTo(Crocodoc.SCROLL_PREVIOUS);
	                } else if (ev.keyCode === 39) {
	                    viewer.scrollTo(Crocodoc.SCROLL_NEXT);
	                } else {
	                    return;
	                }
	                ev.preventDefault();
	            });
	            $(".boxView .leftArrow").click(function(){
	            	viewer.scrollTo(Crocodoc.SCROLL_PREVIOUS);
	            });
	            $(".boxView .rightArrow").click(function(){
	            	viewer.scrollTo(Crocodoc.SCROLL_NEXT);
	            });
	            viewer.setLayout(Crocodoc.LAYOUT_PRESENTATION);
	            viewer.zoom(Crocodoc.ZOOM_AUTO);
	            $('body').removeClass().addClass('crocodoc-presentation-pop');

	        });
	        /* jshint ignore:end */
		});
		
	}

	//********* Copy to Clipboard Functionality ***********//
	if(pathname === "/iconography.html" || pathname === "/color.html"){
		$.getScript( "vendor/ZeroClipboard/ZeroClipboard.min.js", function() {
	    	/* jshint ignore:start */
			ZeroClipboard.config( { swfPath: "../vendor/ZeroClipboard/ZeroClipboard.swf" } );
			var client = new ZeroClipboard($(".copyToClipboard"));
			client.on( "copy", function (event) {
				var clipContent = $(event.target).html();
				
				var clipboard = event.clipboardData;
				clipboard.setData( "text/plain", clipContent );
			});
			/* jshint ignore:end */

			/*var currentDiv = "";

			$( ".sg-color-swatch").click(function(event) {

				currentDiv = $(event.target).attr("class");
				
				if(currentDiv === "sg-color-swatch"){
					$(".sg-animated", this).slideToggle(200);
				}
			});*/
		});

	}

	//********* Iconography ***********//
	if(pathname === "/iconography.html"){
		
        $(".icon-group").click(function(e){
        	$(".icon-group").removeClass("expanded");

        	$(e.currentTarget).addClass("expanded");
        });

	}

	//********* Color Theory ***********//
	if(pathname === "/color.html"){
		
			var currentDiv = "";

			$( ".sg-color-swatch").click(function(event) {

				currentDiv = $(event.target).attr("class");
				
				if(currentDiv === "sg-color-swatch"){
					$(".sg-animated", this).slideToggle(200);
				}
			});        
        /*$.getScript( "vendor/ZeroClipboard/ZeroClipboard.min.js", function() {
			ZeroClipboard.config( { swfPath: "../vendor/ZeroClipboard/ZeroClipboard.swf" } );
			var client = new ZeroClipboard($(".copyColor"));


		});*/
	}
	
}); //document.ready




