/*jslint browser:true*/
/*jshint latedef:false, unused:false*/
/*global Dragend:true, H5F:true, dragendInstance:true */

/* ==========================================================================
 * script.js
 * Javascript configurations and initializers
 * ========================================================================== */

$(document).ready(function() {

	$('#welcomeModal').modal("show");

	var dragendInstance;
	var width = getInnerWidth(); 
	var oldWidth;
	var container;
	var country_str;
	var region;
	var isMobile;
	
	initWindowSize(width);

	$(window).bind('resizeEnd', function() {
    	oldWidth = width; 
		var newWidth = getInnerWidth();
		
		width = newWidth; //update innerWidth DOM property to new width

		if ( (oldWidth > 768) && (newWidth < 768) ) {
			initDragend();
			window.scrollTo(0, 0);
			unwrapDesktopForm();
			wrapSectionForms();
		}
		else if ( (newWidth > 768) && (oldWidth < 768) ) {		
			destroyDragend();
			unwrapSectionForms();
			wrapDesktopForm();
		}
	});

	// show hide Terumo BCT Contact based on user role
	$('#submitterRole').on('change', function(e){
		
		var role = $('#submitterRole').val();
		if(role === 'Associate'){
			$('.terumo-contact').hide();
			$('.progress-item:nth-child(4)').hide();
		}else{
			$('.terumo-contact').show();
			$('.progress-item').show();
		}
	});


	/* redirect somewhere if "save and exit" button is clicked from success modal */
	$('#closeAndExit').on('click', function(e) {
    	$('.dragend-page').hide(); //hide form since they are done with it.
    	$('.alert-success').text('Thank you for using the Terumo BCT Quality Action Tool.').slideDown().fadeIn();
    	//e.preventDefault();
	});

 	$('#callUsButton').on('click' , function(e) {
		$("#callUsModal").modal("show");	
	}); 	

	//indicate the form(s) for validator to act on
	$('#complaint-form').validator();
	$('#formSubmitterInformation, #formIncidentInformation, #formFacilityInformation, #formTerumoContact, #formProductInfo').validator();

		/* reload the page if "submit another incident" button is clicked from success modal */
	$('body').on("click", '#submitAnother', function() {
    	//Clear values of most form fields while retaining submitter info to make another submission a bit easier
		$('#incidentDate').val('');
		$('#incidentProcedureStep').val('');
		$('#incidentOtherProcedureStep').val('');
		$('#incidentDeath').val('');
		$('#incidentInjury').val('');
		$('#incidentMedicalIntervention').val('');
		$('#incidentComments').val('');
		$('#incidentFacilityname').val('');
		$('#incidentFacilitycontact').val('');
		$('#incidentFacilitystreet').val('');
		$('#incidentFacilitycity').val('');
		$('#incidentFacilitystateprovinceterritory').val('');
		$('#incidentFacilitycountry').val('');
		$('#files').html('');

		$('#bctContactfirstname').val('');
		$('#bctContactlastname').val('');
		$('#bctContactphone').val('');
		$('#bctContactemail').val('');

		$('#productName').val('');
		$('#otherProductName').val('');
		$('#productReturnable').val('');
		$('#prodSerialNum').val('');
		$('#prodSoftwareRev').val('');
		$('#prodCatalogNum').val('');
		$('#prodLotNum').val('');
		$('#prodSetNum').val('');
		
		//for desktop
   		window.scrollTo(0, 0); 	
	
	});

}); //end document.ready()

/* FUNCTION DEFINITIONS */

//another resize def 
/*$(window).resize(function() {
    if(this.resizeTO) {
    	clearTimeout(this.resizeTO);
    }
    this.resizeTO = setTimeout(function() {
        //$(this).trigger('resizeEnd'); 
    }, 500);
});*/


//return current viewport width
var getInnerWidth = function () {
 	var w = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
	return w;
};


//wrap each .dragend-page with a form tag for section by section validation
var wrapSectionForms = function () {
 	$('.submitter-information').wrap('<form id="formSubmitterInformation" role="form" novalidate></form>');
 	$('.incident-information').wrap('<form id="formIncidentInformation" role="form" novalidate></form>');
 	$('.facility-information').wrap('<form id="formFacilityInformation" role="form" novalidate></form>');
 	$('.terumo-contact').wrap('<form id="formTerumoContact" role="form" novalidate></form>');
 	$('.product-info').wrap('<form id="formProductInfo" role="form" novalidate></form>');
};


//unwrap section forms around each .dragend-page for desktop viewport widths
var unwrapSectionForms = function () {
 	$('.submitter-information').unwrap();
 	$('.incident-information').unwrap();
 	$('.facility-information').unwrap();
 	$('.terumo-contact').unwrap();
 	$('.product-info').unwrap();
};


//wrap the whole #dragend-container in a form for desktop widths
var wrapDesktopForm = function () {
 	$('#dragend-container').wrap('<form id="complaint-form" role="form" novalidate></form>');
};


//unwrap form around #dragend-container for mobile viewport widths
var unwrapDesktopForm = function () {
 	$('#dragend-container').unwrap();
};


//initialize everyone!
var initWindowSize = function (w) {
	var width = w;
	var windowSmall = 768;
	var windowMedium = 992;
	var windowLarge = 1200; 

	isMobile = mobileCheck();
	
	//isMobile = true; //for toggling between forced desktop and mobile views in development

	if (isMobile) {
		$('.desktop-submit').hide();
		initDragend();
		wrapSectionForms();
	}
	else {
		$('.responsive-nav-buttons').hide();
		wrapDesktopForm();
	}
	init();
};

var mobileCheck = function() {
  var check = false;
  (function(a){
  	if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4)))check = true;})(navigator.userAgent||navigator.vendor||window.opera);
  return check; 
};

//general initializations regardless of whether dragend is enabled or not (viewport agnostic)
var init = function () {
	//country/region mapping
	var country_str = "<option value=\"Afghanistan\" data-alternative-spellings=\"AF افغانستان\" data-region=\"EMEA\">Afghanistan</option>\n<option value=\"Åland Islands\" data-alternative-spellings=\"AX Aaland Aland\" data-relevancy-booster=\"0.5\" data-region=\"EMEA\">Åland Islands</option>\n<option value=\"Albania\" data-alternative-spellings=\"AL\" data-region=\"EMEA\">Albania</option>\n<option value=\"Algeria\" data-alternative-spellings=\"DZ الجزائر\" data-region=\"EMEA\">Algeria</option>\n<option value=\"American Samoa\" data-alternative-spellings=\"AS\" data-relevancy-booster=\"0.5\" data-region=\"Asia Pacific\">American Samoa</option>\n<option value=\"Andorra\" data-alternative-spellings=\"AD\" data-relevancy-booster=\"0.5\" data-region=\"EMEA\">Andorra</option>\n<option value=\"Angola\" data-alternative-spellings=\"AO\" data-region=\"EMEA\">Angola</option>\n<option value=\"Anguilla\" data-alternative-spellings=\"AI\" data-relevancy-booster=\"0.5\" data-region=\"Latin America\">Anguilla</option>\n<option value=\"Antarctica\" data-alternative-spellings=\"AQ\" data-relevancy-booster=\"0.5\" data-region=\"North America\">Antarctica</option>\n<option value=\"Antigua And Barbuda\" data-alternative-spellings=\"AG\" data-relevancy-booster=\"0.5\" data-region=\"Latin America\">Antigua And Barbuda</option>\n<option value=\"Argentina\" data-alternative-spellings=\"AR\" data-region=\"Latin America\">Argentina</option>\n<option value=\"Armenia\" data-alternative-spellings=\"AM Հայաստան\" data-region=\"EMEA\">Armenia</option>\n<option value=\"Aruba\" data-alternative-spellings=\"AW\" data-relevancy-booster=\"0.5\" data-region=\"Latin America\">Aruba</option>\n<option value=\"Australia\" data-alternative-spellings=\"AU\" data-relevancy-booster=\"1.5\" data-region=\"Asia Pacific\">Australia</option>\n<option value=\"Austria\" data-alternative-spellings=\"AT Österreich Osterreich Oesterreich \" data-region=\"EMEA\">Austria</option>\n<option value=\"Azerbaijan\" data-alternative-spellings=\"AZ\" data-region=\"EMEA\">Azerbaijan</option>\n<option value=\"Bahamas\" data-alternative-spellings=\"BS\" data-region=\"Latin America\">Bahamas</option>\n<option value=\"Bahrain\" data-alternative-spellings=\"BH البحرين\" data-region=\"EMEA\">Bahrain</option>\n<option value=\"Bangladesh\" data-alternative-spellings=\"BD বাংলাদেশ\" data-relevancy-booster=\"2\" data-region=\"Asia Pacific\">Bangladesh</option>\n<option value=\"Barbados\" data-alternative-spellings=\"BB\" data-region=\"Latin America\">Barbados</option>\n<option value=\"Belarus\" data-alternative-spellings=\"BY Беларусь\" data-region=\"EMEA\">Belarus</option>\n<option value=\"Belgium\" data-alternative-spellings=\"BE België Belgie Belgien Belgique\" data-relevancy-booster=\"1.5\" data-region=\"EMEA\">Belgium</option>\n<option value=\"Belize\" data-alternative-spellings=\"BZ\" data-region=\"Latin America\">Belize</option>\n<option value=\"Benin\" data-alternative-spellings=\"BJ\" data-region=\"EMEA\">Benin</option>\n<option value=\"Bermuda\" data-alternative-spellings=\"BM\" data-relevancy-booster=\"0.5\" data-region=\"Latin America\">Bermuda</option>\n<option value=\"Bhutan\" data-alternative-spellings=\"BT भूटान\" data-region=\"Asia Pacific\">Bhutan</option>\n<option value=\"Bolivia\" data-alternative-spellings=\"BO\" data-region=\"Latin America\">Bolivia</option>\n<option value=\"Bonaire, Sint Eustatius and Saba\" data-alternative-spellings=\"BQ\" data-region=\"Latin America\">Bonaire, Sint Eustatius and Saba</option>\n<option value=\"Bosnia and Herzegovina\" data-alternative-spellings=\"BA BiH Bosna i Hercegovina Босна и Херцеговина\" data-region=\"EMEA\">Bosnia and Herzegovina</option>\n<option value=\"Botswana\" data-alternative-spellings=\"BW\" data-region=\"EMEA\">Botswana</option>\n<option value=\"Bouvet Island\" data-alternative-spellings=\"BV\" data-region=\"EMEA\">Bouvet Island</option>\n<option value=\"Brazil\" data-alternative-spellings=\"BR Brasil\" data-relevancy-booster=\"2\" data-region=\"Latin America\">Brazil</option>\n<option value=\"British Indian Ocean Territory\" data-alternative-spellings=\"IO\" data-region=\"Asia Pacific\">British Indian Ocean Territory</option>\n<option value=\"Brunei Darussalam\" data-alternative-spellings=\"BN\" data-region=\"Asia Pacific\">Brunei Darussalam</option>\n<option value=\"Bulgaria\" data-alternative-spellings=\"BG България\" data-region=\"EMEA\">Bulgaria</option>\n<option value=\"Burkina Faso\" data-alternative-spellings=\"BF\" data-region=\"EMEA\">Burkina Faso</option>\n<option value=\"Burundi\" data-alternative-spellings=\"BI\" data-region=\"EMEA\">Burundi</option>\n<option value=\"Cambodia\" data-alternative-spellings=\"KH កម្ពុជា\" data-region=\"EMEA\">Cambodia</option>\n<option value=\"Cameroon\" data-alternative-spellings=\"CM\" data-region=\"EMEA\">Cameroon</option>\n<option value=\"Canada\" data-alternative-spellings=\"CA\" data-relevancy-booster=\"2\" data-region=\"North America\">Canada</option>\n<option value=\"Cape Verde\" data-alternative-spellings=\"CV Cabo\" data-region=\"EMEA\">Cape Verde</option>\n<option value=\"Cayman Islands\" data-alternative-spellings=\"KY\" data-relevancy-booster=\"0.5\" data-region=\"Latin America\">Cayman Islands</option>\n<option value=\"Central African Republic\" data-alternative-spellings=\"CF\" data-region=\"EMEA\">Central African Republic</option>\n<option value=\"Chad\" data-alternative-spellings=\"TD تشاد‎ Tchad\" data-region=\"EMEA\">Chad</option>\n<option value=\"Chile\" data-alternative-spellings=\"CL\" data-region=\"Latin America\">Chile</option>\n<option value=\"China\" data-relevancy-booster=\"3.5\" data-alternative-spellings=\"CN Zhongguo Zhonghua Peoples Republic 中国/中华\" data-region=\"Asia Pacific\">China</option>\n<option value=\"Christmas Island\" data-alternative-spellings=\"CX\" data-relevancy-booster=\"0.5\" data-region=\"Asia Pacific\">Christmas Island</option>\n<option value=\"Cocos (Keeling) Islands\" data-alternative-spellings=\"CC\" data-relevancy-booster=\"0.5\" data-region=\"Asia Pacific\">Cocos (Keeling) Islands</option>\n<option value=\"Colombia\" data-alternative-spellings=\"CO\" data-region=\"Latin America\">Colombia</option>\n<option value=\"Comoros\" data-alternative-spellings=\"KM جزر القمر\" data-region=\"EMEA\">Comoros</option>\n<option value=\"Congo\" data-alternative-spellings=\"CG\" data-region=\"EMEA\">Congo</option>\n<option value=\"Congo, the Democratic Republic of the\" data-alternative-spellings=\"CD Congo-Brazzaville Repubilika ya Kongo\" data-region=\"EMEA\">Congo, the Democratic Republic of the</option>\n<option value=\"Cook Islands\" data-alternative-spellings=\"CK\" data-relevancy-booster=\"0.5\" data-region=\"Asia Pacific\">Cook Islands</option>\n<option value=\"Costa Rica\" data-alternative-spellings=\"CR\" data-region=\"Latin America\">Costa Rica</option>\n<option value=\"Côte d'Ivoire\" data-alternative-spellings=\"CI Cote dIvoire\" data-region=\"EMEA\">Côte d'Ivoire</option>\n<option value=\"Croatia\" data-alternative-spellings=\"HR Hrvatska\" data-region=\"EMEA\">Croatia</option>\n<option value=\"Cuba\" data-alternative-spellings=\"CU\" data-region=\"Latin America\">Cuba</option>\n<option value=\"Curaçao\" data-alternative-spellings=\"CW Curacao\" data-region=\"Latin America\">Curaçao</option>\n<option value=\"Cyprus\" data-alternative-spellings=\"CY Κύπρος Kýpros Kıbrıs\" data-region=\"EMEA\">Cyprus</option>\n<option value=\"Czech Republic\" data-alternative-spellings=\"CZ Česká Ceska\" data-region=\"EMEA\">Czech Republic</option>\n<option value=\"Denmark\" data-alternative-spellings=\"DK Danmark\" data-relevancy-booster=\"1.5\" data-region=\"EMEA\">Denmark</option>\n<option value=\"Djibouti\" data-alternative-spellings=\"DJ جيبوتي‎ Jabuuti Gabuuti\" data-region=\"EMEA\">Djibouti</option>\n<option value=\"Dominica\" data-alternative-spellings=\"DM Dominique\" data-relevancy-booster=\"0.5\" data-region=\"Latin America\">Dominica</option>\n<option value=\"Dominican Republic\" data-alternative-spellings=\"DO\" data-region=\"Latin America\">Dominican Republic</option>\n<option value=\"Ecuador\" data-alternative-spellings=\"EC\" data-region=\"Latin America\">Ecuador</option>\n<option value=\"Egypt\" data-alternative-spellings=\"EG\" data-relevancy-booster=\"1.5\" data-region=\"EMEA\">Egypt</option>\n<option value=\"El Salvador\" data-alternative-spellings=\"SV\" data-region=\"Latin America\">El Salvador</option>\n<option value=\"Equatorial Guinea\" data-alternative-spellings=\"GQ\" data-region=\"EMEA\">Equatorial Guinea</option>\n<option value=\"Eritrea\" data-alternative-spellings=\"ER إرتريا ኤርትራ\" data-region=\"EMEA\">Eritrea</option>\n<option value=\"Estonia\" data-alternative-spellings=\"EE Eesti\" data-region=\"EMEA\">Estonia</option>\n<option value=\"Ethiopia\" data-alternative-spellings=\"ET ኢትዮጵያ\" data-region=\"EMEA\">Ethiopia</option>\n<option value=\"Falkland Islands (Malvinas)\" data-alternative-spellings=\"FK\" data-relevancy-booster=\"0.5\" data-region=\"Latin America\">Falkland Islands (Malvinas)</option>\n<option value=\"Faroe Islands\" data-alternative-spellings=\"FO Føroyar Færøerne\" data-relevancy-booster=\"0.5\" data-region=\"EMEA\">Faroe Islands</option>\n<option value=\"Fiji\" data-alternative-spellings=\"FJ Viti फ़िजी\" data-region=\"Asia Pacific\">Fiji</option>\n<option value=\"Finland\" data-alternative-spellings=\"FI Suomi\" data-region=\"EMEA\">Finland</option>\n<option value=\"France\" data-alternative-spellings=\"FR République française\" data-relevancy-booster=\"2.5\" data-region=\"EMEA\">France</option>\n<option value=\"French Guiana\" data-alternative-spellings=\"GF\" data-region=\"Latin America\">French Guiana</option>\n<option value=\"French Polynesia\" data-alternative-spellings=\"PF Polynésie française\" data-region=\"Asia Pacific\">French Polynesia</option>\n<option value=\"French Southern Territories\" data-alternative-spellings=\"TF\" data-region=\"Asia Pacific\">French Southern Territories</option>\n<option value=\"Gabon\" data-alternative-spellings=\"GA République Gabonaise\" data-region=\"EMEA\">Gabon</option>\n<option value=\"Gambia\" data-alternative-spellings=\"GM\" data-region=\"EMEA\">Gambia</option>\n<option value=\"Georgia\" data-alternative-spellings=\"GE საქართველო\" data-region=\"EMEA\">Georgia</option>\n<option value=\"Germany\" data-alternative-spellings=\"DE Bundesrepublik Deutschland\" data-relevancy-booster=\"3\" data-region=\"EMEA\">Germany</option>\n<option value=\"Ghana\" data-alternative-spellings=\"GH\" data-region=\"EMEA\">Ghana</option>\n<option value=\"Gibraltar\" data-alternative-spellings=\"GI\" data-relevancy-booster=\"0.5\" data-region=\"EMEA\">Gibraltar</option>\n<option value=\"Greece\" data-alternative-spellings=\"GR Ελλάδα\" data-relevancy-booster=\"1.5\" data-region=\"EMEA\">Greece</option>\n<option value=\"Greenland\" data-alternative-spellings=\"GL grønland\" data-relevancy-booster=\"0.5\" data-region=\"EMEA\">Greenland</option>\n<option value=\"Grenada\" data-alternative-spellings=\"GD\" data-region=\"Latin America\">Grenada</option>\n<option value=\"Guadeloupe\" data-alternative-spellings=\"GP\" data-region=\"Latin America\">Guadeloupe</option>\n<option value=\"Guam\" data-alternative-spellings=\"GU\" data-region=\"Latin America\">Guam</option>\n<option value=\"Guatemala\" data-alternative-spellings=\"GT\" data-region=\"Latin America\">Guatemala</option>\n<option value=\"Guernsey\" data-alternative-spellings=\"GG\" data-relevancy-booster=\"0.5\" data-region=\"EMEA\">Guernsey</option>\n<option value=\"Guinea\" data-alternative-spellings=\"GN\" data-region=\"EMEA\">Guinea</option>\n<option value=\"Guinea-Bissau\" data-alternative-spellings=\"GW\" data-region=\"EMEA\">Guinea-Bissau</option>\n<option value=\"Guyana\" data-alternative-spellings=\"GY\" data-region=\"Latin America\">Guyana</option>\n<option value=\"Haiti\" data-alternative-spellings=\"HT\" data-region=\"Latin America\">Haiti</option>\n<option value=\"Heard Island and McDonald Islands\" data-alternative-spellings=\"HM\" data-region=\"EMEA\">Heard Island and McDonald Islands</option>\n<option value=\"Holy See (Vatican City State)\" data-alternative-spellings=\"VA\" data-relevancy-booster=\"0.5\" data-region=\"EMEA\">Holy See (Vatican City State)</option>\n<option value=\"Honduras\" data-alternative-spellings=\"HN\" data-region=\"Latin America\">Honduras</option>\n<option value=\"Hong Kong\" data-alternative-spellings=\"HK 香港\" data-region=\"Asia Pacific\">Hong Kong</option>\n<option value=\"Hungary\" data-alternative-spellings=\"HU Magyarország\" data-region=\"EMEA\">Hungary</option>\n<option value=\"Iceland\" data-alternative-spellings=\"IS Island\" data-region=\"EMEA\">Iceland</option>\n<option value=\"India\" data-alternative-spellings=\"IN भारत गणराज्य Hindustan\" data-relevancy-booster=\"3\" data-region=\"Asia Pacific\">India</option>\n<option value=\"Indonesia\" data-alternative-spellings=\"ID\" data-relevancy-booster=\"2\" data-region=\"Asia Pacific\">Indonesia</option>\n<option value=\"Iran, Islamic Republic of\" data-alternative-spellings=\"IR ایران\" data-region=\"EMEA\">Iran, Islamic Republic of</option>\n<option value=\"Iraq\" data-alternative-spellings=\"IQ العراق‎\" data-region=\"EMEA\">Iraq</option>\n<option value=\"Ireland\" data-alternative-spellings=\"IE Éire\" data-relevancy-booster=\"1.2\" data-region=\"EMEA\">Ireland</option>\n<option value=\"Isle of Man\" data-alternative-spellings=\"IM\" data-relevancy-booster=\"0.5\" data-region=\"EMEA\">Isle of Man</option>\n<option value=\"Israel\" data-alternative-spellings=\"IL إسرائيل ישראל\" data-region=\"EMEA\">Israel</option>\n<option value=\"Italy\" data-alternative-spellings=\"IT Italia\" data-relevancy-booster=\"2\" data-region=\"EMEA\">Italy</option>\n<option value=\"Jamaica\" data-alternative-spellings=\"JM\" data-region=\"Latin America\">Jamaica</option>\n<option value=\"Japan\" data-alternative-spellings=\"JP Nippon Nihon 日本 にほん\" data-relevancy-booster=\"2.5\" data-region=\"Japan\">Japan</option>\n<option value=\"Jersey\" data-alternative-spellings=\"JE\" data-relevancy-booster=\"0.5\" data-region=\"EMEA\">Jersey</option>\n<option value=\"Jordan\" data-alternative-spellings=\"JO الأردن\" data-region=\"EMEA\">Jordan</option>\n<option value=\"Kazakhstan\" data-alternative-spellings=\"KZ Қазақстан Казахстан\" data-region=\"EMEA\">Kazakhstan</option>\n<option value=\"Kenya\" data-alternative-spellings=\"KE\" data-region=\"EMEA\">Kenya</option>\n<option value=\"Kiribati\" data-alternative-spellings=\"KI\" data-region=\"Asia Pacific\">Kiribati</option>\n<option value=\"Korea, Democratic People's Republic of\" data-alternative-spellings=\"KP North Korea\" data-region=\"Asia Pacific\">Korea, Democratic People's Republic of</option>\n<option value=\"Korea, Republic of\" data-alternative-spellings=\"KR South Korea\" data-relevancy-booster=\"1.5\" data-region=\"Asia Pacific\">Korea, Republic of</option>\n<option value=\"Kuwait\" data-alternative-spellings=\"KW الكويت\" data-region=\"EMEA\">Kuwait</option>\n<option value=\"Kyrgyzstan\" data-alternative-spellings=\"KG Кыргызстан\" data-region=\"EMEA\">Kyrgyzstan</option>\n<option value=\"Lao People's Democratic Republic\" data-alternative-spellings=\"LA\" data-region=\"Asia Pacific\">Lao People's Democratic Republic</option>\n<option value=\"Latvia\" data-alternative-spellings=\"LV Latvija\" data-region=\"EMEA\">Latvia</option>\n<option value=\"Lebanon\" data-alternative-spellings=\"LB لبنان\" data-region=\"EMEA\">Lebanon</option>\n<option value=\"Lesotho\" data-alternative-spellings=\"LS\" data-region=\"EMEA\">Lesotho</option>\n<option value=\"Liberia\" data-alternative-spellings=\"LR\" data-region=\"EMEA\">Liberia</option>\n<option value=\"Libyan Arab Jamahiriya\" data-alternative-spellings=\"LY ليبيا\" data-region=\"EMEA\">Libyan Arab Jamahiriya</option>\n<option value=\"Liechtenstein\" data-alternative-spellings=\"LI\" data-region=\"EMEA\">Liechtenstein</option>\n<option value=\"Lithuania\" data-alternative-spellings=\"LT Lietuva\" data-region=\"EMEA\">Lithuania</option>\n<option value=\"Luxembourg\" data-alternative-spellings=\"LU\" data-region=\"EMEA\">Luxembourg</option>\n<option value=\"Macao\" data-alternative-spellings=\"MO Macau\" data-region=\"Asia Pacific\">Macao</option>\n<option value=\"Macedonia, The Former Yugoslav Republic Of\" data-alternative-spellings=\"MK Македонија\" data-region=\"EMEA\">Macedonia, The Former Yugoslav Republic Of</option>\n<option value=\"Madagascar\" data-alternative-spellings=\"MG Madagasikara\" data-region=\"EMEA\">Madagascar</option>\n<option value=\"Malawi\" data-alternative-spellings=\"MW\" data-region=\"EMEA\">Malawi</option>\n<option value=\"Malaysia\" data-alternative-spellings=\"MY\" data-region=\"Asia Pacific\">Malaysia</option>\n<option value=\"Maldives\" data-alternative-spellings=\"MV\" data-region=\"Asia Pacific\">Maldives</option>\n<option value=\"Mali\" data-alternative-spellings=\"ML\" data-region=\"EMEA\">Mali</option>\n<option value=\"Malta\" data-alternative-spellings=\"MT\" data-region=\"EMEA\">Malta</option>\n<option value=\"Marshall Islands\" data-alternative-spellings=\"MH\" data-relevancy-booster=\"0.5\" data-region=\"Asia Pacific\">Marshall Islands</option>\n<option value=\"Martinique\" data-alternative-spellings=\"MQ\" data-region=\"Latin America\">Martinique</option>\n<option value=\"Mauritania\" data-alternative-spellings=\"MR الموريتانية\" data-region=\"EMEA\">Mauritania</option>\n<option value=\"Mauritius\" data-alternative-spellings=\"MU\" data-region=\"EMEA\">Mauritius</option>\n<option value=\"Mayotte\" data-alternative-spellings=\"YT\" data-region=\"EMEA\">Mayotte</option>\n<option value=\"Mexico\" data-alternative-spellings=\"MX Mexicanos\" data-relevancy-booster=\"1.5\" data-region=\"Latin America\">Mexico</option>\n<option value=\"Micronesia, Federated States of\" data-alternative-spellings=\"FM\" data-region=\"Asia Pacific\">Micronesia, Federated States of</option>\n<option value=\"Moldova, Republic of\" data-alternative-spellings=\"MD\" data-region=\"EMEA\">Moldova, Republic of</option>\n<option value=\"Monaco\" data-alternative-spellings=\"MC\" data-region=\"EMEA\">Monaco</option>\n<option value=\"Mongolia\" data-alternative-spellings=\"MN Mongγol ulus Монгол улс\" data-region=\"Asia Pacific\">Mongolia</option>\n<option value=\"Montenegro\" data-alternative-spellings=\"ME\" data-region=\"EMEA\">Montenegro</option>\n<option value=\"Montserrat\" data-alternative-spellings=\"MS\" data-relevancy-booster=\"0.5\" data-region=\"Latin America\">Montserrat</option>\n<option value=\"Morocco\" data-alternative-spellings=\"MA المغرب\" data-region=\"EMEA\">Morocco</option>\n<option value=\"Mozambique\" data-alternative-spellings=\"MZ Moçambique\" data-region=\"EMEA\">Mozambique</option>\n<option value=\"Myanmar\" data-alternative-spellings=\"MM\" data-region=\"Asia Pacific\">Myanmar</option>\n<option value=\"Namibia\" data-alternative-spellings=\"North America Namibië\" data-region=\"EMEA\">Namibia</option>\n<option value=\"Nauru\" data-alternative-spellings=\"NR Naoero\" data-relevancy-booster=\"0.5\" data-region=\"Asia Pacific\">Nauru</option>\n<option value=\"Nepal\" data-alternative-spellings=\"NP नेपाल\" data-region=\"Asia Pacific\">Nepal</option>\n<option value=\"Netherlands\" data-alternative-spellings=\"NL Holland Nederland\" data-relevancy-booster=\"1.5\" data-region=\"EMEA\">Netherlands</option>\n<option value=\"New Caledonia\" data-alternative-spellings=\"NC\" data-relevancy-booster=\"0.5\" data-region=\"Asia Pacific\">New Caledonia</option>\n<option value=\"New Zealand\" data-alternative-spellings=\"NZ Aotearoa\" data-region=\"Asia Pacific\">New Zealand</option>\n<option value=\"Nicaragua\" data-alternative-spellings=\"NI\" data-region=\"Latin America\">Nicaragua</option>\n<option value=\"Niger\" data-alternative-spellings=\"NE Nijar\" data-region=\"EMEA\">Niger</option>\n<option value=\"Nigeria\" data-alternative-spellings=\"NG Nijeriya Naíjíríà\" data-relevancy-booster=\"1.5\" data-region=\"EMEA\">Nigeria</option>\n<option value=\"Niue\" data-alternative-spellings=\"NU\" data-relevancy-booster=\"0.5\" data-region=\"Asia Pacific\">Niue</option>\n<option value=\"Norfolk Island\" data-alternative-spellings=\"NF\" data-relevancy-booster=\"0.5\" data-region=\"Asia Pacific\">Norfolk Island</option>\n<option value=\"Northern Mariana Islands\" data-alternative-spellings=\"MP\" data-relevancy-booster=\"0.5\" data-region=\"Asia Pacific\">Northern Mariana Islands</option>\n<option value=\"Norway\" data-alternative-spellings=\"NO Norge Noreg\" data-relevancy-booster=\"1.5\" data-region=\"EMEA\">Norway</option>\n<option value=\"Oman\" data-alternative-spellings=\"OM عمان\" data-region=\"EMEA\">Oman</option>\n<option value=\"Pakistan\" data-alternative-spellings=\"PK پاکستان\" data-relevancy-booster=\"2\" data-region=\"EMEA\">Pakistan</option>\n<option value=\"Palau\" data-alternative-spellings=\"PW\" data-relevancy-booster=\"0.5\" data-region=\"Asia Pacific\">Palau</option>\n<option value=\"Palestinian Territory, Occupied\" data-alternative-spellings=\"PS فلسطين\" data-region=\"EMEA\">Palestinian Territory, Occupied</option>\n<option value=\"Panama\" data-alternative-spellings=\"PA\" data-region=\"Latin America\">Panama</option>\n<option value=\"Papua New Guinea\" data-alternative-spellings=\"PG\" data-region=\"Asia Pacific\">Papua New Guinea</option>\n<option value=\"Paraguay\" data-alternative-spellings=\"PY\" data-region=\"Latin America\">Paraguay</option>\n<option value=\"Peru\" data-alternative-spellings=\"PE\" data-region=\"Latin America\">Peru</option>\n<option value=\"Philippines\" data-alternative-spellings=\"PH Pilipinas\" data-relevancy-booster=\"1.5\" data-region=\"Asia Pacific\">Philippines</option>\n<option value=\"Pitcairn\" data-alternative-spellings=\"PN\" data-relevancy-booster=\"0.5\" data-region=\"Asia Pacific\">Pitcairn</option>\n<option value=\"Poland\" data-alternative-spellings=\"PL Polska\" data-relevancy-booster=\"1.25\" data-region=\"EMEA\">Poland</option>\n<option value=\"Portugal\" data-alternative-spellings=\"PT Portuguesa\" data-relevancy-booster=\"1.5\" data-region=\"EMEA\">Portugal</option>\n<option value=\"Puerto Rico\" data-alternative-spellings=\"PR\" data-region=\"Latin America\">Puerto Rico</option>\n<option value=\"Qatar\" data-alternative-spellings=\"QA قطر\" data-region=\"EMEA\">Qatar</option>\n<option value=\"Réunion\" data-alternative-spellings=\"RE Reunion\" data-region=\"EMEA\">Réunion</option>\n<option value=\"Romania\" data-alternative-spellings=\"RO Rumania Roumania România\" data-region=\"EMEA\">Romania</option>\n<option value=\"Russian Federation\" data-alternative-spellings=\"RU Rossiya Российская Россия\" data-relevancy-booster=\"2.5\" data-region=\"EMEA\">Russian Federation</option>\n<option value=\"Rwanda\" data-alternative-spellings=\"RW\" data-region=\"EMEA\">Rwanda</option>\n<option value=\"Saint Barthélemy\" data-alternative-spellings=\"BL St. Barthelemy\" data-region=\"Latin America\">Saint Barthélemy</option>\n<option value=\"Saint Helena\" data-alternative-spellings=\"SH St.\" data-region=\"EMEA\">Saint Helena</option>\n<option value=\"Saint Kitts and Nevis\" data-alternative-spellings=\"KN St.\" data-region=\"Latin America\">Saint Kitts and Nevis</option>\n<option value=\"Saint Lucia\" data-alternative-spellings=\"LC St.\" data-region=\"Latin America\">Saint Lucia</option>\n<option value=\"Saint Martin (French Part)\" data-alternative-spellings=\"MF St.\" data-region=\"Latin America\">Saint Martin (French Part)</option>\n<option value=\"Saint Pierre and Miquelon\" data-alternative-spellings=\"PM St.\" data-region=\"North America\">Saint Pierre and Miquelon</option>\n<option value=\"Saint Vincent and the Grenadines\" data-alternative-spellings=\"VC St.\" data-region=\"Latin America\">Saint Vincent and the Grenadines</option>\n<option value=\"Samoa\" data-alternative-spellings=\"WS\" data-region=\"Asia Pacific\">Samoa</option>\n<option value=\"San Marino\" data-alternative-spellings=\"SM RSM Repubblica\" data-region=\"EMEA\">San Marino</option>\n<option value=\"Sao Tome and Principe\" data-alternative-spellings=\"ST\" data-region=\"EMEA\">Sao Tome and Principe</option>\n<option value=\"Saudi Arabia\" data-alternative-spellings=\"SA السعودية\" data-region=\"EMEA\">Saudi Arabia</option>\n<option value=\"Senegal\" data-alternative-spellings=\"SN Sénégal\" data-region=\"EMEA\">Senegal</option>\n<option value=\"Serbia\" data-alternative-spellings=\"RS Србија Srbija\" data-region=\"EMEA\">Serbia</option>\n<option value=\"Seychelles\" data-alternative-spellings=\"SC\" data-relevancy-booster=\"0.5\" data-region=\"EMEA\">Seychelles</option>\n<option value=\"Sierra Leone\" data-alternative-spellings=\"SL\" data-region=\"EMEA\">Sierra Leone</option>\n<option value=\"Singapore\" data-alternative-spellings=\"SG Singapura  சிங்கப்பூர் குடியரசு 新加坡共和国\" data-region=\"Asia Pacific\">Singapore</option>\n<option value=\"Sint Maarten (Dutch Part)\" data-alternative-spellings=\"SX\" data-region=\"Latin America\">Sint Maarten (Dutch Part)</option>\n<option value=\"Slovakia\" data-alternative-spellings=\"SK Slovenská Slovensko\" data-region=\"EMEA\">Slovakia</option>\n<option value=\"Slovenia\" data-alternative-spellings=\"SI Slovenija\" data-region=\"EMEA\">Slovenia</option>\n<option value=\"Solomon Islands\" data-alternative-spellings=\"SB\" data-region=\"Asia Pacific\">Solomon Islands</option>\n<option value=\"Somalia\" data-alternative-spellings=\"SO الصومال\" data-region=\"EMEA\">Somalia</option>\n<option value=\"South Africa\" data-alternative-spellings=\"ZA RSA Suid-Afrika\" data-region=\"EMEA\">South Africa</option>\n<option value=\"South Georgia and the South Sandwich Islands\" data-alternative-spellings=\"GS\" data-region=\"Latin America\">South Georgia and the South Sandwich Islands</option>\n<option value=\"South Sudan\" data-alternative-spellings=\"SS\" data-region=\"EMEA\">South Sudan</option>\n<option value=\"Spain\" data-alternative-spellings=\"ES España\" data-relevancy-booster=\"2\" data-region=\"EMEA\">Spain</option>\n<option value=\"Sri Lanka\" data-alternative-spellings=\"LK ශ්‍රී ලංකා இலங்கை Ceylon\" data-region=\"Asia Pacific\">Sri Lanka</option>\n<option value=\"Sudan\" data-alternative-spellings=\"SD السودان\" data-region=\"EMEA\">Sudan</option>\n<option value=\"Suriname\" data-alternative-spellings=\"SR शर्नम् Sarnam Sranangron\" data-region=\"Latin America\">Suriname</option>\n<option value=\"Svalbard and Jan Mayen\" data-alternative-spellings=\"SJ\" data-relevancy-booster=\"0.5\" data-region=\"EMEA\">Svalbard and Jan Mayen</option>\n<option value=\"Swaziland\" data-alternative-spellings=\"SZ weSwatini Swatini Ngwane\" data-region=\"EMEA\">Swaziland</option>\n<option value=\"Sweden\" data-alternative-spellings=\"SE Sverige\" data-relevancy-booster=\"1.5\" data-region=\"EMEA\">Sweden</option>\n<option value=\"Switzerland\" data-alternative-spellings=\"CH Swiss Confederation Schweiz Suisse Svizzera Svizra\" data-relevancy-booster=\"1.5\" data-region=\"EMEA\">Switzerland</option>\n<option value=\"Syrian Arab Republic\" data-alternative-spellings=\"SY Syria سورية\" data-region=\"EMEA\">Syrian Arab Republic</option>\n<option value=\"Taiwan, Province of China\" data-alternative-spellings=\"TW 台灣 臺灣\" data-region=\"Asia Pacific\">Taiwan, Province of China</option>\n<option value=\"Tajikistan\" data-alternative-spellings=\"TJ Тоҷикистон Toçikiston\" data-region=\"EMEA\">Tajikistan</option>\n<option value=\"Tanzania, United Republic of\" data-alternative-spellings=\"TZ\" data-region=\"EMEA\">Tanzania, United Republic of</option>\n<option value=\"Thailand\" data-alternative-spellings=\"TH ประเทศไทย Prathet Thai\" data-region=\"Asia Pacific\">Thailand</option>\n<option value=\"Timor-Leste\" data-alternative-spellings=\"TL\" data-region=\"Asia Pacific\">Timor-Leste</option>\n<option value=\"Togo\" data-alternative-spellings=\"TG Togolese\" data-region=\"EMEA\">Togo</option>\n<option value=\"Tokelau\" data-alternative-spellings=\"TK\" data-relevancy-booster=\"0.5\" data-region=\"Asia Pacific\">Tokelau</option>\n<option value=\"Tonga\" data-alternative-spellings=\"TO\" data-region=\"Asia Pacific\">Tonga</option>\n<option value=\"Trinidad and Tobago\" data-alternative-spellings=\"TT\" data-region=\"Latin America\">Trinidad and Tobago</option>\n<option value=\"Tunisia\" data-alternative-spellings=\"TN تونس\" data-region=\"EMEA\">Tunisia</option>\n<option value=\"Turkey\" data-alternative-spellings=\"TR Türkiye Turkiye\" data-region=\"EMEA\">Turkey</option>\n<option value=\"Turkmenistan\" data-alternative-spellings=\"TM Türkmenistan\" data-region=\"EMEA\">Turkmenistan</option>\n<option value=\"Turks and Caicos Islands\" data-alternative-spellings=\"TC\" data-relevancy-booster=\"0.5\" data-region=\"Latin America\">Turks and Caicos Islands</option>\n<option value=\"Tuvalu\" data-alternative-spellings=\"TV\" data-relevancy-booster=\"0.5\" data-region=\"Asia Pacific\">Tuvalu</option>\n<option value=\"Uganda\" data-alternative-spellings=\"UG\" data-region=\"EMEA\">Uganda</option>\n<option value=\"Ukraine\" data-alternative-spellings=\"UA Ukrayina Україна\" data-region=\"EMEA\">Ukraine</option>\n<option value=\"United Arab Emirates\" data-alternative-spellings=\"AE UAE الإمارات\" data-region=\"EMEA\">United Arab Emirates</option>\n<option value=\"United Kingdom\" data-alternative-spellings=\"GB Great Britain England UK Wales Scotland Northern Ireland\" data-relevancy-booster=\"2.5\" data-region=\"EMEA\">United Kingdom</option>\n<option value=\"United States\" data-relevancy-booster=\"3.5\" data-alternative-spellings=\"US USA United States of America\" data-region=\"North America\">United States</option>\n<option value=\"United States Minor Outlying Islands\" data-alternative-spellings=\"UM\" data-region=\"North America\">United States Minor Outlying Islands</option>\n<option value=\"Uruguay\" data-alternative-spellings=\"UY\" data-region=\"Latin America\">Uruguay</option>\n<option value=\"Uzbekistan\" data-alternative-spellings=\"UZ Ўзбекистон O'zbekstan O‘zbekiston\" data-region=\"EMEA\">Uzbekistan</option>\n<option value=\"Vanuatu\" data-alternative-spellings=\"VU\" data-region=\"Asia Pacific\">Vanuatu</option>\n<option value=\"Venezuela\" data-alternative-spellings=\"VE\" data-region=\"Latin America\">Venezuela</option>\n<option value=\"Vietnam\" data-alternative-spellings=\"VN Việt Nam\" data-relevancy-booster=\"1.5\" data-region=\"Asia Pacific\">Vietnam</option>\n<option value=\"Virgin Islands, British\" data-alternative-spellings=\"VG\" data-relevancy-booster=\"0.5\" data-region=\"Latin America\">Virgin Islands, British</option>\n<option value=\"Virgin Islands, U.S.\" data-alternative-spellings=\"VI\" data-relevancy-booster=\"0.5\" data-region=\"North America\">Virgin Islands, U.S.</option>\n<option value=\"Wallis and Futuna\" data-alternative-spellings=\"WF\" data-relevancy-booster=\"0.5\" data-region=\"Asia Pacific\">Wallis and Futuna</option>\n<option value=\"Western Sahara\" data-alternative-spellings=\"EH لصحراء الغربية\" data-region=\"EMEA\">Western Sahara</option>\n<option value=\"Yemen\" data-alternative-spellings=\"YE اليمن\" data-region=\"EMEA\">Yemen</option>\n<option value=\"Zambia\" data-alternative-spellings=\"ZM\" data-region=\"EMEA\">Zambia</option>\n<option value=\"Zimbabwe\" data-alternative-spellings=\"ZW\" data-region=\"EMEA\">Zimbabwe</option>";
    $( "#submitterCountry" ).append( $(country_str) );
    $( "#submitterCountry" ).selectToAutocomplete();
    $( "#submitterCountry" ).change(function() {		
		var region = $('option:selected', "#submitterCountry").attr('data-region');
	});
    //apply typeahead ux functionality to facility country as well.
	$( "#incidentFacilitycountry" ).append( $(country_str) );
    $( "#incidentFacilitycountry" ).selectToAutocomplete();

	$('.ui-autocomplete-input').closest('.form-group').removeClass('has-error');
	//setup method for H5F polyfill to add HTML5 form chapters to IE9 and older browsers  ZJ NO IDEA IF THIS WORKS (ZJ)
	if (document.getElementById('complaint-form')) {
		H5F.setup(document.getElementById('complaint-form')); 
	}
	else {
		H5F.setup(document.getElementById('formSubmitterInformation')); 
		H5F.setup(document.getElementById('formIncidentInformation')); 
		H5F.setup(document.getElementById('formFacilityInformation')); 
		H5F.setup(document.getElementById('formTerumoContact')); 
		H5F.setup(document.getElementById('formProductInfo')); 
	}
 	//initialize tooltip hints
	$('[data-toggle="tooltip"]').tooltip();

	$('#submitterAwarenessDate').datepicker({ maxDate: new Date() } );
	$('#incidentDate').datepicker({ maxDate: new Date() });
	//popup for serious reported events
	$('body').on('click', '#incidentDeathYes, #incidentInjuryYes, #incidentMedicalInterventionYes', function(e) {
		$("#emergencyCallUsModal").modal("show");
	});

	/*awareness date and incident date needs to be "readonly" so that only the datepicker can be utilized to enter a date. 
	This avoids an international date format (dd/mm/yyyy) from being entered and misinterpreted as a mm/dd/yyyy 
	format. Readonly must be removed on focusout so that the validator can style this field when it is invalid (aka empty) */ 
	/*$(document).on('focusin', '#submitterAwarenessDate', function(e) {
  		$(this).prop('readonly', true);
	});
	$(document).on('focusout', '#submitterAwarenessDate', function(e) {
  		$(this).prop('readonly', false);
	});
	$(document).on('focusin', '#incidentDate', function(e) {
  		$(this).prop('readonly', true);
	});
	$(document).on('focusout', '#incidentDate', function(e) {
  		$(this).prop('readonly', false);
	});*/

	//conditionally hide/show form fields for 'other' selection in procedure step and product name dropdowns
		$('select[name=productname]').change(function () {
	    if ($(this).val() === 'other') {
	        $('#otherProductNameRow').slideDown();
	        $('select[name=productname]').removeAttr('required');
	        $('#otherProductName').attr('required',true);
	    } else {
	        $('#otherProductNameRow').slideUp();
	        $('select[name=productname]').attr('required',true);
	        $('#otherProductName').removeAttr('required').parent().removeClass('has-error');
	    }
	});
	$('select[name=incidentprocedurestep]').change(function () {
	    if ($(this).val() === 'other') {
	        $('#incidentOtherProcedureStepRow').slideDown();
	        $('select[name=incidentprocedurestep]').removeAttr('required');
	        $('#incidentOtherProcedureStep').attr('required',true);
	    } else {
	        $('#incidentOtherProcedureStepRow').slideUp();
	        $('select[name=incidentprocedurestep]').attr('required',true);
	        $('#incidentOtherProcedureStep').removeAttr('required').parent().removeClass('has-error');
	    }
	});

	//file upload 
	/*var url = window.location.href;
	var arr = url.split("/");
	var currentURL = arr[0] + "//" + arr[2];
	var uploadButton;

	url = currentURL + '/_vti_bin/BCT.Ext.Services/FormToListService.svc/SaveFile?list=/Lists/GlobalComplaints&itemId=401&updateToken=7a6f2e9d-fb60-48bf-af70-1f7112124936';
	uploadButton = $('<span/>')
	.addClass('glyphicon glyphicon-ok fileGood fileIndicator')
	.prop('disabled', true);

	$('#fileupload').fileupload({
		url: url,
		dataType: 'json',
		autoUpload: false,
		acceptFileTypes: /(\.|\/)(gif|jpe?g|png|xls|pdf|doc|ppt|xlsx|docx|pptx|txt|csv)$/i,
	maxFileSize: 5000000, // 5 MB
	sequentialUploads: true,
	}).on('fileuploadadd', function (e, data) {
		if($("#files .fileGood").length <= 4){
			data.context = $('<div/>').appendTo('#files');
			$.each(data.files, function (index, file) {
				var node = $('<p/>')
				.append(uploadButton.clone(true).data(data))
				.append($('<span/>').text(file.name))
				.append('<br>');
				if (!index) {
				}
				node.appendTo(data.context);
			});
		}else{
			alert("Sorry. Only five attachments are allowed.");
		}
		
	}).on('fileuploadprocessalways', function (e, data) {
		var index = data.index,
		file = data.files[index],
		node = $(data.context.children()[index]);

		if (file.error) {
			node
			.append($('<span class="text-danger"/>').text(file.error));

			data.context.find('.glyphicon')
			.removeClass('fileGood')
			.removeClass('glyphicon-ok')
			.addClass('fileBad')
			.addClass('glyphicon-remove');
		}
		if (index + 1 === data.files.length) {
			data.context.find('.glyphicon')
			.prop('disabled', !!data.files.error);
		}
	}).on('fileuploaddone', function (e, data) {
		$.each(data.result.files, function (index, file) {
			if (file.url) {
				var link = $('<a>')
				.attr('target', '_blank')
				.prop('href', file.url);
				$(data.context.children()[index])
				.wrap(link);
			} else if (file.error) {
				var error = $('<span class="text-danger"/>').text(file.error);
				$(data.context.children()[index])
				.append('<br>')
				.append(error);
			}
		});
	}).on('fileuploadfail', function (e, data) {
		$.each(data.files, function (index) {
			var error = $('<span class="text-danger"/>').text('File upload failed.');
			$(data.context.children()[index])
			.append('<br>')
			.append(error);
		});
	}).prop('disabled', !$.support.fileInput)
	.parent().addClass($.support.fileInput ? undefined : 'disabled');*/

};


//initiailize dragend for swipe and next/back functionality
var initDragend = function () {
	container = document.getElementById('dragend-container');
	dragendInstance = new Dragend(container, {
        afterInitialize: function() {
         	container.style.visibility = 'visible';
         	//init(); 
        },
        preventDrag: true
    });

    window.addEventListener('resize', function() {
    	dragendInstance._sizePages();
	});

    $('body').on('click', '.next', function() {
    	//dragendInstance.swipe('left');
	});
	$('body').on('click', '.back', function(e) {
    	dragendInstance.swipe('right');
    	e.stopPropagation(); //stop bubbling
	});
	$('body').on("click", '#submitAnother', function() {
		dragendInstance.swipe(1); //take user to 'page 1' to submit another
	});

};

var destroyDragend = function () {
	dragendInstance.destroy();
	//init();
};

//scroll listener for desktop progress indicaton

var isMobile = mobileCheck();

if (!isMobile) {

	$(window).scroll(function() {
		var that = this;
	    var bottom_of_window = $(window).scrollTop() + 140;// + $(window).height();
		var bottom_of_submitter = $('.submitter-information').offset().top + $('.submitter-information').outerHeight();
		var bottom_of_product = $('.product-info').offset().top + $('.product-info').outerHeight();
		var bottom_of_incident = $('.incident-information').offset().top + $('.incident-information').outerHeight();
		var bottom_of_contact = $('.terumo-contact').offset().top + $('.terumo-contact').outerHeight();
		var bottom_of_facility = $('.facility-information').offset().top + $('.facility-information').outerHeight();
		if($(window).scrollTop() + $(window).height() === $(document).height()) { bottom_of_facility = 0;}

		var current = getCurrentWindow(bottom_of_window,bottom_of_submitter,bottom_of_product,bottom_of_incident,bottom_of_contact,bottom_of_facility);
		
		//console.log(bottom_of_window, bottom_of_submitter, bottom_of_product, bottom_of_incident, bottom_of_contact, bottom_of_facility)

		switch (current){
			case "submitter":
	    		$(".progress-item").removeClass("current");
				$(".progress-item:nth-child(1)").addClass("current");
			 break;
			case "product":
				$(".progress-item").removeClass("current");
				$(".progress-item:nth-child(2)").addClass("current");
			 break;
			case "incident":
				$(".progress-item").removeClass("current");
				$(".progress-item:nth-child(3)").addClass("current");
			 break;
			case "terumo":
				if(notAssociate()){
					$(".progress-item").removeClass("current");
					$(".progress-item:nth-child(4)").addClass("current");
				}
			 break;
			case "facility":
				$(".progress-item").removeClass("current");
				$(".progress-item:last-child").addClass("current");
			 break;
			default:
		}

		function getCurrentWindow(B,s,p,i,c,f){
			if (B < s) { 
				return "submitter"; 
			}
			if (B < p) { 
				return "product"; 
			}
			if ( notAssociate() ) {
				if (B < i) { 
					return "incident"; 
				}
				if (B < c) { 
					return "terumo"; 
				}
			} else {
				if (B < i) { return "incident"; }
			}
			if(B < f) { return "facility"; }

			/*if(B < s && B < p) return "submitter";
			if(B > p && B < i) return "product";
			if(notAssociate()){
				if(B > i && B < c) return "incident";
				if(B > c && B < f) return "terumo";
			}else{
				if(B > i && B < f) return "incident";
			}
			if(B > f) return "facility";*/
		}
		function notAssociate(){
			if($('#submitterRole').val() !== "Associate"){
				return true;
			}else{
				return false;
			}
		}

	});
}


