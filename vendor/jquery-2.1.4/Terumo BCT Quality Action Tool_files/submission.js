/*jshint latedef:nofunc, unused:false*/
/*globals Dragend:true, H5F:true, $:true */

/* ==========================================================================
 * validation for mobile view and desktop view 
 * ========================================================================== */

$(document).ready(function() {

	var submissionContainer; //for use in displaying validation error in desktop. store reference to whether form was submitted via a form section (mobile) or a grand, single form submission (desktop)

	$('#formSubmitterInformation, #formIncidentInformation, #formFacilityInformation, #formTerumoContact, #formProductInfo').validator().on('submit', function (e) {
		
		var form = this;
		var currentId = $(this).attr('id');

		var progressForward = false;

		debugger
		if (e.isDefaultPrevented()) {			
			$('html, body').animate({ scrollTop: 0 }, 300, 
				function() {
					$(e.delegateTarget).find('.alert-danger').text('Please fill out all required fields.').slideDown().fadeIn();
			});

			e.preventDefault(); 
		}	
		else { 			
			e.preventDefault();

			switch(currentId){
				case "formSubmitterInformation":
					updateCompleted(1);
					updateCurrent(2);
				 break;
				case "formProductInfo":
					updateCompleted(2);
					updateCurrent(3);
				 break;
				case "formIncidentInformation":
					updateCompleted(3);
					updateCurrent(4);
				 break;
				case "formTerumoContact":

						updateCompleted(4);
						updateCurrent(5);
				 break;
				case "formFacilityInformation":
					updateCompleted(5);
					//if(!notAssociate()){
						updateCompleted(4);
					//}
				 break;
				default:
			}
			
			dragendInstance.swipe('left');

			window.scrollTo(0, 0);
			$(e.delegateTarget).find('.alert-danger').hide();
			if ($(this).attr('id') === 'formFacilityInformation') {				
				processForm();
			}
		}

		function updateCurrent(childNum){
			$(".progress-item").removeClass("current");
			$(".progress-item:nth-child("+childNum+")").addClass("current");
		}

		function updateCompleted(childNum){
			$(".progress-item").removeClass("current");
			$(".progress-item:nth-child("+childNum+")").addClass("completed");
		}

		function notAssociate(){
			if($('#submitterRole').val() != "Associate"){
				return true;
			}else{
				return false;
			}
		}

	});


	$('#complaint-form').validator().on('submit', function (e) {
		if (e.isDefaultPrevented()) {	

			//console.log(isMobile);		
			
			//old style of validation error notification
			/*$('html, body').animate({ 
				scrollTop: 0 }, 300, 
			function() {
				$('.desktop-validation-alert').text('Please fill out all required fields.').slideDown().fadeIn();
			});*/

			//scroll to first invalid field on submission
			$('html, body').animate({
		    	scrollTop: $('.has-error:visible:first').offset().top-120		    
			}, 500);

			//e.preventDefault();
		}
		else {
			processForm();
			e.preventDefault();	
		}	
	});

	var processForm = function (e) {

		//console.log('validation success');

		var opts = {
			lines: 13, // The number of lines to draw
			length: 20, // The length of each line
			width: 10, // The line thickness
			radius: 30, // The radius of the inner circle
			corners: 1, // Corner roundness (0..1)
			rotate: 0, // The rotation offset
			direction: 1, // 1: clockwise, -1: counterclockwise
			color: '#000', // #rgb or #rrggbb or array of colors
			speed: 1, // Rounds per second
			trail: 60, // Afterglow percentage
			shadow: false, // Whether to render a shadow
			hwaccel: false, // Whether to use hardware acceleration
			className: 'spinner', // The CSS class to assign to the spinner
			zIndex: 2e9, // The z-index (defaults to 2000000000)
			top: 'auto', // Top position relative to parent in px
			left:'auto' // Left position relative to parent in px
		};

		var target = document.getElementById('searching_spinner_center');
		var spinner = new Spinner(opts).spin(target);

		$("#Searching_Modal").modal("show");	

		$('.alert-danger').hide();
		//concatenate some values for Title list readability
		var Title = 
			$('#incidentDate').val() + ' - ' + 
			$('#productName').val() + ' - ' +  
			$('#incidentFacilityname').val();
		//conditionally assign value to procedure step depending on whether or not 'other' was selected in dropdown
		var incidentProcedureStep;
		if ($('#incidentProcedureStep').val() === 'other') {
			incidentProcedureStep = $('#incidentOtherProcedureStep').val();	
		}
		else {
			incidentProcedureStep = $('#incidentProcedureStep').val();
		}
		//conditionally assign value to product depending on whether or not 'other' was selected in dropdown
		var productName;
		if ($('#productName').val() === 'other') {
			productName = $('#otherProductName').val();	
		}
		else {
			productName = $('#productName').val();
		}
		//convert date formats to ISO 8601 to avoid error in submitting to SharePoint list
		var incidentDate = new Date( $('#incidentDate').val() );
		//console.log('non iso date is ' + incidentDate);
		var isoIncidentDate = incidentDate.toISOString();
		//console.log('iso conversion of date is: ' + isoIncidentDate);

		var awarenessDate = new Date( $('#submitterAwarenessDate').val() );
		//console.log('non iso date is ' + awarenessDate);
		var isoAwarenessDate = awarenessDate.toISOString();
		//console.log('iso conversion of date is: ' + isoAwarenessDate);

		var complaintList = {
			
			"Title"										: Title,
			"submitterCountry"							: $('#submitterCountry').val(),
			"submitterRegion"							: $('option:selected', "#submitterCountry").attr('data-region'),
			"submitterRole"								: $('#submitterRole').val(),
			"submitterFirstname"						: $('#submitterFirstname').val(),
			"submitterLastname"							: $('#submitterLastname').val(),
			"submitterPhone"							: $('#submitterPhone').val(),
			"submitterEmail"							: $('#submitterEmail').val(),
			"awareDate"									: isoAwarenessDate,
									
			"incidentDate"								: isoIncidentDate,
			"incidentProcedureStep"						: incidentProcedureStep,
			"incidentDeath"								: $("input:radio[name='incidentdeath']:checked").val(),
			"incidentInjury"							: $("input:radio[name='incidentInjury']:checked").val(),
			"incidentMedicalIntervention"				: $("input:radio[name='incidentmedicalintervention']:checked").val(),
			"incidentComments"							: $('#incidentComments').val(),
			"incidentFacilityname"						: $('#incidentFacilityname').val(),
			"incidentFacilitycontact"					: $('#incidentFacilitycontact').val(),
			"incidentFacilitystreet"					: $('#incidentFacilitystreet').val(),
			"incidentFacilitycity"						: $('#incidentFacilitycity').val(),
			"incidentFacilitystateprovinceterritory"	: $('#incidentFacilitystateprovinceterritory').val(),
			"incidentFacilitycountry"					: $('#incidentFacilitycountry').val(),

			"bctContactfirstname"						: $('#bctContactfirstname').val(),
			"bctContactlastname"						: $('#bctContactlastname').val(),
			"bctContactphone"							: $('#bctContactphone').val(),
			"bctContactemail"							: $('#bctContactemail').val(),

			"productName"								: productName,
			"productReturnable"							: $("input:radio[name='productReturnable']:checked").val(),
			"prodSerialNum"								: $('#prodSerialNum').val(),
			"prodSoftwareRev"							: $('#prodSoftwareRev').val(),
			"prodCatalogNum"							: $('#prodCatalogNum').val(),
			"prodLotNum"								: $('#prodLotNum').val(),
			"prodSetNum"								: $('#prodSetNum').val()

		};

		//quick and dirty test to check for native JSON in browser
		if (typeof(JSON) === 'object' && typeof(JSON.parse) === 'function') {
			//console.log('Native JSON parsing is available');
		}
		
	    var json = JSON.stringify(complaintList); //json2 serialization script for olde timey browsers (pre IE8 and IE8 running in non standards mode, but lets talk about this)

	    var url = window.location.href;
		var arr = url.split("/");
		var currentURL = arr[0] + "//" + arr[2];

	    //SaveItem service call
	    $.ajax({
	        type: 'POST',		        
	        url: currentURL + '/_vti_bin/BCT.Ext.Services/FormToListService.svc/SaveItem?list=/Lists/QATool',
	        data: json,
	        contentType: 'application/json; charset=utf-8',
	        dataType: 'json',
	        async: false, 
	        error: function(){
		        $("#errorModal").modal("show");
		        //e.preventDefault();
		 	},
	        success: function(SaveItemResponse) {

				var newURL = currentURL + '/_vti_bin/BCT.Ext.Services/FormToListService.svc/SaveFile?list=/Lists/QATool&itemId=' + SaveItemResponse.ItemId + '&updateToken=' + SaveItemResponse.UpdateToken;	    
		    	
		    	$('#fileupload').fileupload(
				    'option',
				    'url',
				    newURL
				);

		    	var count = $(".fileGood").length;
		    	if(count === 0){sendingDone();} //changed from equality operator to identity operator ZJ April 27
				
				function sendingDone(){

					$("#Searching_Modal").on("hidden.bs.modal", function () {
				  		$('.alert-danger').text(''); //hide previous validation error(s) if it is still visible.
						$('.desktop-validation-alert').text(''); //hide previous desktop validation.
						$("#successModal").modal("show");	 	
					})
					$("#Searching_Modal").modal("hide");
				}

				$(".fileGood").each(function() {
					var $this = $(this);//, data = ;
					var myData = $(this).data();

					myData.submit().always(function () {
					  $this.remove();
					  if (!--count){
					  	sendingDone();
					  }
					});
				});
				//e.preventDefault(); //prevent the form from reloading the page in the classic way when the form is successfully submitted
	        }
	    }); // SaveItem .ajax
		//e.preventDefault();
	}; //processForm()

}); //$(document).ready


