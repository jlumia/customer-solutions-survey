//enter keypress to add group
app.directive('enter', function () {
    return function (scope, element, attrs) {
        
        //enter key when creating new group
        element.bind("keypress", function (event) {
            if (event.which === 13) {
                scope.addGroup();
                scope.$apply();
            }
        });
    };
});

//each item in list
app.directive('item', function () {
    return {
        restrict: 'A',
        link: function (scope, element) {

            //mouse/drag events
            element.bind("mousedown", function () {
                element.css('cursor', 'grabbing');
                element.css('cursor', '-webkit-grabbing');
            });
            element.bind("mouseup", function () {
                element.css('cursor', 'grab');
                element.css('cursor', '-webkit-grab');
            });

            element.bind("dragend", function () {
                element.css('cursor', 'grab');
                element.css('cursor', '-webkit-grab');
            });

            //dragstart event
            element.bind("dragstart", function (e) {

                //get the data
                var data = element[0].getElementsByClassName('info')[0].innerHTML;

                //set the data
                e.dataTransfer.setData("text", data);
            });
        }
    };
});

//add item to new group
app.directive('addtogroup', function () {
    return {
        restrict: 'A',
        link: function (scope, element) {

            //dragover event
            element.bind('dragover', function (e) {

                //add copy icon to drop area
                e.preventDefault();
                //e.dataTransfer.dropEffect = 'copy';

                element.css("background", scope.hoverColor);
            });

            //dragleave event
            element.bind('dragleave', function () {

                //revert back to normal color
                element.css("background", scope.returnColor);
            });

            //drop event
            element.bind('drop', function (e) {

                //prevent page nav
                e.preventDefault();

                //return color back to normal
                element.css("background", scope.returnColor);

                //get data
                var data = e.dataTransfer.getData("text");

                //get title
                var title = element[0].getElementsByClassName('groupTitle')[0].innerHTML;

                if (scope.groupObj[title].items.indexOf(data) !== -1) {
                    return;
                }

                //push to respective group
                scope.groupObj[title].items.push(data);

                //apply scope
                scope.$apply();

            });
        }
    };
});