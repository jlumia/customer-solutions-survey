var app = angular.module("pgs", []);

app.controller("itemController", function ($scope, $sce, $http) {

    //escape chars
    $scope.escape = function (value) {
        return $sce.trustAsHtml(value);
    };

    //colors
    $scope.returnColor = "#fff";
    $scope.hoverColor = "#ccc";

    //items list
    $scope.items = [
        'Atreus&reg; Whole Blood Processing System',
        'FloGenix Automated Opener',
        'IMUFLEX&reg; Blood Bags',
        'InfoVu',
        'Mirasol&reg; Pathogen Reduction Technology',
        'OrbiSac System',
        'Outcome Review',
        'Reveos&reg; Automated Blood Processing System',
        'T-ACE II+ (Terumo Automatic Component Extractor)',
        'TACSI&reg; (Terumo Automated Centrifuge & Separator Integration)',
        'TERUFLEX Blood Bag System',
        'TOMEs (Terumo Operational Medical Equipment Software)',
        'T-RAC II (Terumo Recording Automatic Component and Blood Collector)',
        'Trima Accel&reg; Automated Blood Collection System',
        'Trucise&reg; 2.0 Total System',
        'T-SEAL II Tube Sealing Device',
        'TSCD&reg; II Sterile Tubing Welder',
        'Vista&reg; Information System',
        'Harvest&reg; AdiPrep&reg; Procedure Pack',
        'Harvest&reg; APC+&reg; Procedure Pack',
        'Harvest&reg; BMAC&reg; (Bone Marrow Aspirate Concentrate) Procedure Pack',
        'Harvest&reg; Clear PRP Procedure Kit',
        'COBE&reg; 2991 Cell Processor',
        'Elutra&reg; Cell Separation System',
        'Quantum&reg; Cell Expansion System',
        'SCD&reg; IIB Sterile Tubing Welder',
        'Harvest&reg; SmartPrep&reg; Multicellular Processing System',
        'TSCD&reg;-Q Sterile Tubing Welder',
        'Cadence&reg; Data Collection System',
        'COBE&reg; Spectra Apheresis System',
        'Spectra Optia&reg;  Apheresis System'
    ];

    //group name
    $scope.groupName = '';

    //object to store groups
    $scope.groupObj = {};

    //boolean to show edit group prompt
    $scope.showPrompt = true;

    //edit groups 
    $scope.editGroups = function () {
        $scope.showPrompt = true;
    };

    //function to add group
    $scope.addGroup = function () {

        //bail out if no input
        if ($scope.groupName.length === 0) {
            return;
        }

        //add new group
        $scope.groupObj[$scope.groupName] = {
            items: []
        };

        //reset groupName
        $scope.groupName = '';
    };

    //remove group
    $scope.removeGroup = function (g) {
        delete $scope.groupObj[g];
    };

    //remove item
    $scope.removeItem = function (g, i) {
        $scope.groupObj[g].items.splice($scope.groupObj[g].items.indexOf(i), 1);
    };

    //submit button
    $scope.submit = function () {

        //convert group obj to json string
        var json = JSON.stringify($scope.groupObj);
        console.log(json)

        //get the textarea element and assign json
        document.getElementById("element_0").value = json;

        //get the form and submit it
        document.getElementById("emf-form").submit();

    };

});